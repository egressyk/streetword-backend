import {Document, model, Model, Schema} from 'mongoose';
import {IMessageSchema} from "./message";

const userSchema = new Schema({
    username: {type: String, required: true},
    email: {type: String, required: true, index: true},
    messages: [{type: Schema.Types.ObjectId, ref: 'Message'}],
    creationDate: {type: Date, default: Date.now()}
});

userSchema.index({email: 1});

export interface IUserSchema extends IUser, Document {}

export interface IUser {
    id?: any;
    username: string;
    email: string;
    creationDate?: Date;
    messages?: IMessageSchema[];
}

export const User: Model<IUserSchema> = model('User', userSchema);