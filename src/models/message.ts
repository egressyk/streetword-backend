import {Document, Model, model, Schema} from 'mongoose';


const messageSchema = new Schema({
    latitude: {type: Number, required: true, index: true},
    longitude: {type: Number, required: true, index: true},
    author: {type: Schema.Types.ObjectId, required: true, ref: 'User'},
    content: {type: String, required: true},
    creationDate: {type: Date, default: Date.now},
    votes: {type: Map, of: Boolean, default: {}}
});

messageSchema.index({latitude: 1, longitude: 1});

export interface IMessageSchema extends IMessage, Document {}

export interface IMessage {
    id?: any;
    latitude: number;
    longitude: number;
    author: any;
    content: string;
    creationDate?: Date;
    votes?: Map<string, boolean>;
}

export const Message: Model<IMessageSchema> = model('Message', messageSchema);