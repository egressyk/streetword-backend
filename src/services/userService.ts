import {IUser, IUserSchema, User} from "../models/user";
import request, {RequestPromise} from "request-promise-native";

export class UserService {

    getAllUsers(): Promise<IUserSchema[]> {
        return User.find()
            .select('_id username email creationDate')
            .exec();
    }

    getUserByID(userId: string): Promise<IUserSchema | null>{
        return User.findById(userId).exec();
    }

    creatUser(userData: IUser): Promise<IUserSchema> {
        const user = new User({
            username: userData.username,
            email: userData.email
        });
        return user.save()
    }

    loginUserWithFacebookToken(token: string): Promise<IUserSchema[]> {
        return this.getUserDataFromFacebookByToken(token)
            .then((userData) => {
                return User.find({email: userData.email}).exec();
            });
    }

    getUserDataFromFacebookByToken(token: string): RequestPromise<any> {
        return request(`https://graph.facebook.com/v3.1/me?fields=email&access_token=${token}`, {json: true});
    }

    deleteUser(userId: string): Promise<IUserSchema | null> {
        return User.findByIdAndRemove(userId).exec();
    }

    async creatUserWithFacebookToken(userData: {username: string, token:string}): Promise<any>  {
        const userFBData = await this.getUserDataFromFacebookByToken(userData.token);
        const newUser: IUser = {
            username: userData.username,
            email: userFBData.email
        };
        return this.creatUser(newUser);
    }
}