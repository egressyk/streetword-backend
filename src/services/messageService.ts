import {IMessage, IMessageSchema, Message} from "../models/message";
import {IUserSchema, User} from "../models/user";


export class MessageService {
    getAllMessages(): Promise<IMessageSchema[]> {
        return Message.find()
            .populate("author", "username")
            .exec();
    }

    getMessagesWithinArea(location: {longitude: number, latitude: number}, radius: number){

    }

    getMessageByID(messageId: string): Promise<IMessageSchema | null> {
        return Message.findById(messageId)
            .populate("author", "username")
            .exec();
    }

    getMessagesByUserID(userId: string): Promise<IUserSchema | null> {
        return User.findById(userId).populate('messages').exec();
    }

    creatMessage(messageData: IMessage): Promise<IMessageSchema> {
        const message = new Message({
            latitude: messageData.latitude,
            longitude: messageData.longitude,
            author: messageData.author,
            content: messageData.content
        });
        return message.save()
            .then((result) => {
                User.findByIdAndUpdate(messageData.author, {$push: {messages: message._id}}).exec();
                return result;
            });
    }

    deleteMessage(messageId: string): Promise<IMessageSchema | null> {
        return Message.findByIdAndRemove(messageId).exec();
    }

    deleteMessagesByUserID(userId: string): Promise<any[]> {
        return User.findByIdAndUpdate(userId, {$set: {messages: []}}).exec()
            .then((result) => {
                if (result !== null) {
                    return Message.remove({author: userId}).exec();
                } else {
                    return null;
                }
            });
    }

    voteForMessage(messageId: string, userId: string, vote: boolean): Promise<IMessageSchema | null> {
        let queryArgument: any = {};
        let key = `votes.${userId}`;
        queryArgument[key] = vote;
        console.log(queryArgument);
        return Message.findByIdAndUpdate(messageId, {$set: queryArgument}, {new: true})
            .populate("author", "username")
            .exec();
    }
}