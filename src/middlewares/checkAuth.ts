import {NextFunction, Request, Response} from "express";
import * as jwt from "jsonwebtoken";

export function checkAuthentication(req: Request, res: Response, next: NextFunction) {
    try {
        const token = (req.headers.authorization as string).split(" ")[1];
        const decodedTokenData = <any>jwt.verify(token, <string>process.env.JWT_KEY);
        req.body.userId = decodedTokenData.id;
        next();
    } catch (error) {
        return res.status(401).json({
            error: {
                message: "Authentication failed"
            }
        })
    }
}