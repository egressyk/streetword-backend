import {NextFunction, Request, Response} from "express";

export function headerManager(req: Request, res: Response, next: NextFunction) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-type, Authorization');
    if (req.method == "OPTIONS") {
        res.sendStatus(200);
    } else {
        next();
    }
}