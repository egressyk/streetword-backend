import {NextFunction, Request, Response, Router} from "express";
import {UserService} from "../services/userService";
import {IUserSchema} from "../models/user";
import * as jwt from "jsonwebtoken";
import {checkAuthentication} from "../middlewares/checkAuth";

class UserController{
    private userService: UserService;
    router: Router;

    constructor(messageService: UserService){
        this.router = Router();
        this.userService = messageService;
        this.setupControllers();
    }

    private setupControllers() {
        this.router.route("/")
            .get(checkAuthentication)
            .get((req: Request, res: Response, next: NextFunction) => {
                this.userService.getAllUsers()
                    .then((result: IUserSchema[]) => {
                        res.status(200).json(this.convertToRestfulResponse(req, result));
                    })
                    .catch((error) => {
                        next(error);
                    });
            })
            .post((req: Request, res: Response, next: NextFunction) => {
                const userData = {
                    username: req.body.username,
                    token: req.body.token
                };
                this.userService.creatUserWithFacebookToken(userData)
                    .then((result: IUserSchema) => {
                        res.status(201).json({
                            token: this.createUserToken(result)
                        });
                    })
                    .catch((error) => {
                        next(error);
                    });
            });

        this.router.route("/login/facebook")
            .post((req: Request, res: Response, next: NextFunction) => {
                this.userService.loginUserWithFacebookToken(req.body.token)
                    .then((result) => {
                        if (result.length > 0) {
                            res.status(200).json({
                                token: this.createUserToken(result[0])
                            });
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No user for token: ${req.body.token}`
                                }
                            });
                            console.log("No user");
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            });

        this.router.route("/:userId")
            .all(checkAuthentication)
            .get((req: Request, res: Response, next: NextFunction) => {
                this.userService.getUserByID(req.params.userId)
                    .then((result)=> {
                        if (result !== null) {
                            res.status(200).json(this.convertToRestfulResponse(req, result));
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No user with id: ${req.params.userId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            })
            .delete((req: Request, res: Response, next: NextFunction) => {
                this.userService.deleteUser(req.params.userId)
                    .then((result) => {
                        if (result !== null) {
                            res.status(200).json(this.convertToRestfulResponse(req, result));
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No user with id: ${req.params.userId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            });
    }

    private convertToRestfulResponse(req: Request, queryData: IUserSchema | IUserSchema[]): any | any[]{
        console.log(queryData);
        if (queryData instanceof Array) {
            const self = this;
            return queryData.map((element) => {
                return self.convertToRestfulResponse.call(self, req, element);
            });
        } else {
            return {
                id: queryData._id,
                username: queryData.username,
                email: queryData.email,
                creationDate: queryData.creationDate,
                link: req.protocol + '://' + req.get('host') + req.baseUrl + '/' + queryData._id,
                messages: queryData.messages
         }
        }
    }

    private createUserToken(userData: IUserSchema): string {
        return jwt.sign({
                id: userData._id,
                email: userData.email
            },
            <string>process.env.JWT_KEY,
            {
                expiresIn: '1h'
            })
    }
}

export const UserRouter = (userService: UserService) => new UserController(userService).router;