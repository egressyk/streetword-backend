import {NextFunction, Request, Response, Router} from "express";
import {MessageService} from "../services/messageService";
import {IMessageSchema} from "../models/message";
import {checkAuthentication} from "../middlewares/checkAuth";

export class MessageController{
    private messageService: MessageService;
    router: Router;

    constructor(messageService: MessageService){
        this.router = Router();
        this.messageService = messageService;
        this.setupControllers();
    }

    private setupControllers() {
        this.router.route("/")
            .all(checkAuthentication)
            .get((req: Request, res: Response, next: NextFunction) => {
                this.messageService.getAllMessages()
                    .then((result: IMessageSchema[]) => {
                        res.status(200).json(this.convertToRestfulResponse(req, result));
                    })
                    .catch((error) => {
                        next(error);
                    });
            })
            .post((req: Request, res: Response, next: NextFunction) => {
                const newMessage = {
                    latitude: req.body.latitude,
                    longitude: req.body.longitude,
                    content: req.body.content,
                    author: req.body.author,
                };
                this.messageService.creatMessage(newMessage)
                    .then((result: IMessageSchema) => {
                        res.status(201)
                            .json(this.convertToRestfulResponse(req, result))
                    })
                    .catch((error) => {
                        next(error);
                    });
            });

        this.router.route("/:messageId")
            .all(checkAuthentication)
            .get((req: Request, res: Response, next: NextFunction) => {
                this.messageService.getMessageByID(req.params.messageId)
                    .then((result)=> {
                        if (result !== null) {
                            res.status(200).json(this.convertToRestfulResponse(req, result));
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No message with id: ${req.params.messageId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            })
            .delete((req: Request, res: Response, next: NextFunction) => {
                this.messageService.deleteMessage(req.params.messageId)
                    .then((result) => {
                        if (result !== null) {
                            res.status(200).json(this.convertToRestfulResponse(req, result));
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No message with id: ${req.params.messageId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            });

        this.router.route("/:messageId/vote")
            .all(checkAuthentication)
            .post((req: Request, res: Response, next: NextFunction) => {
                this.messageService.voteForMessage(req.params.messageId, req.body.userId, req.body.vote)
                    .then((result) => {
                        if (result !== null) {
                            res.status(200).json(this.convertToRestfulResponse(req, result));
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No message with id: ${req.params.messageId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            });

        this.router.route("/user/:userId")
            .all(checkAuthentication)
            .get((req: Request, res: Response, next: NextFunction) => {
                this.messageService.getMessagesByUserID(req.params.userId)
                    .then((result)=> {
                        if (result !== null) {
                            let messages = (result.messages) ? result.messages : [];
                            res.status(200).json(this.convertToRestfulResponse(req, messages));
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No user with id: ${req.params.userId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            })
            .delete((req: Request, res: Response, next: NextFunction) => {
                this.messageService.deleteMessagesByUserID(req.params.userId)
                    .then((result) => {
                        console.log(result);
                        if (result !== null) {
                            res.status(200).json({
                                message: `Deleted all messages of user: ${req.params.userId}`
                            });
                        } else {
                            res.status(404).json({
                                error: {
                                    message: `No user with id: ${req.params.userId}`
                                }
                            });
                        }
                    })
                    .catch((error) => {
                        next(error);
                    });
            });
    }

    private convertToRestfulResponse(req: Request, queryData: IMessageSchema | IMessageSchema[]): any | any[]{
        if (queryData instanceof Array) {
            const self = this;
            return queryData.map((element) => {
                return self.convertToRestfulResponse.call(self, req, element);
            });
        } else {
            return {
                id: queryData._id,
                link: req.protocol + '://' + req.get('host') + req.baseUrl + '/' + queryData._id,
                latitude: queryData.latitude,
                longitude: queryData.longitude,
                content: queryData.content,
                author: {
                    id: queryData.author._id,
                    username: queryData.author.username,
                    link: req.protocol + '://' + req.get('host') + '/users' + '/' + queryData.author._id
                },
                creationDate: queryData.creationDate,
                votes: queryData.votes
            }
        }
    }
}

export const MessageRouter = (messageService: MessageService) => new MessageController(messageService).router;