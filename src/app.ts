import express, {NextFunction, Request, Response} from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import {headerManager} from "./middlewares/headerManager";
import {MessageRouter} from "./controllers/messageController";
import {MessageService} from "./services/messageService";
import {UserRouter} from "./controllers/userController";
import {UserService} from "./services/userService";


const app = express();

setMiddlewares(app);
setRoutes(app);
setErrorHandling(app);

initMongoDatabaseConnection(process.env.MONGO_ATLAS_CONNECTION_LINK || "");



function setMiddlewares(app: express.Application){
    app.use(headerManager);
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(morgan('dev'));

}

function setRoutes(app: express.Application){
    app.get("/", (req, res) => {res.send("StreetWord API server listening for requests")});
    app.use('/messages', MessageRouter(new MessageService()));
    app.use('/users', UserRouter(new UserService()));
}

function setErrorHandling(app: express.Application) {
    app.use((req: Request, res: Response, next: NextFunction) => {
        const err = new Error('Requested location not found');
        err.name = '404';
        next(err);
    });
    app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
        const responseData = {
            error: {
                message: err.message,
                statusCode: (err.name === '404') ? err.name : '500'
            }
        };
        res.status(parseInt(responseData.error.statusCode))
            .send(responseData);
    });
}

function initMongoDatabaseConnection(mongoConnectionLink: string) {
    mongoose.connect(mongoConnectionLink, {useNewUrlParser: true, config: {autoIndex: false}})
        .then(() => console.log('Succesfully connected to database'))
        .catch((err) => console.log(err));
}

export {app}