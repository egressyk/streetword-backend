"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../models/user");
const request_promise_native_1 = __importDefault(require("request-promise-native"));
class UserService {
    getAllUsers() {
        return user_1.User.find()
            .select('_id username email creationDate')
            .exec();
    }
    getUserByID(userId) {
        return user_1.User.findById(userId).exec();
    }
    creatUser(userData) {
        const user = new user_1.User({
            username: userData.username,
            email: userData.email
        });
        return user.save();
    }
    loginUserWithFacebookToken(token) {
        return this.getUserDataFromFacebookByToken(token)
            .then((userData) => {
            return user_1.User.find({ email: userData.email }).exec();
        });
    }
    getUserDataFromFacebookByToken(token) {
        return request_promise_native_1.default(`https://graph.facebook.com/v3.1/me?fields=email&access_token=${token}`, { json: true });
    }
    deleteUser(userId) {
        return user_1.User.findByIdAndRemove(userId).exec();
    }
    creatUserWithFacebookToken(userData) {
        return __awaiter(this, void 0, void 0, function* () {
            const userFBData = yield this.getUserDataFromFacebookByToken(userData.token);
            const newUser = {
                username: userData.username,
                email: userFBData.email
            };
            return this.creatUser(newUser);
        });
    }
}
exports.UserService = UserService;
