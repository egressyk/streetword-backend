"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const message_1 = require("../models/message");
const user_1 = require("../models/user");
class MessageService {
    getAllMessages() {
        return message_1.Message.find()
            .populate("author", "username")
            .exec();
    }
    getMessagesWithinArea(location, radius) {
    }
    getMessageByID(messageId) {
        return message_1.Message.findById(messageId)
            .populate("author", "username")
            .exec();
    }
    getMessagesByUserID(userId) {
        return user_1.User.findById(userId).populate('messages').exec();
    }
    creatMessage(messageData) {
        const message = new message_1.Message({
            latitude: messageData.latitude,
            longitude: messageData.longitude,
            author: messageData.author,
            content: messageData.content
        });
        return message.save()
            .then((result) => {
            user_1.User.findByIdAndUpdate(messageData.author, { $push: { messages: message._id } }).exec();
            return result;
        });
    }
    deleteMessage(messageId) {
        return message_1.Message.findByIdAndRemove(messageId).exec();
    }
    deleteMessagesByUserID(userId) {
        return user_1.User.findByIdAndUpdate(userId, { $set: { messages: [] } }).exec()
            .then((result) => {
            if (result !== null) {
                return message_1.Message.remove({ author: userId }).exec();
            }
            else {
                return null;
            }
        });
    }
    voteForMessage(messageId, userId, vote) {
        let queryArgument = {};
        let key = `votes.${userId}`;
        queryArgument[key] = vote;
        console.log(queryArgument);
        return message_1.Message.findByIdAndUpdate(messageId, { $set: queryArgument }, { new: true })
            .populate("author", "username")
            .exec();
    }
}
exports.MessageService = MessageService;
