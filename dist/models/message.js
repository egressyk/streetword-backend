"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const messageSchema = new mongoose_1.Schema({
    latitude: { type: Number, required: true, index: true },
    longitude: { type: Number, required: true, index: true },
    author: { type: mongoose_1.Schema.Types.ObjectId, required: true, ref: 'User' },
    content: { type: String, required: true },
    creationDate: { type: Date, default: Date.now },
    votes: { type: Map, of: Boolean, default: {} }
});
messageSchema.index({ latitude: 1, longitude: 1 });
exports.Message = mongoose_1.model('Message', messageSchema);
