"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const userSchema = new mongoose_1.Schema({
    username: { type: String, required: true },
    email: { type: String, required: true, index: true },
    messages: [{ type: mongoose_1.Schema.Types.ObjectId, ref: 'Message' }],
    creationDate: { type: Date, default: Date.now() }
});
userSchema.index({ email: 1 });
exports.User = mongoose_1.model('User', userSchema);
