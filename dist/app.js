"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const body_parser_1 = __importDefault(require("body-parser"));
const morgan_1 = __importDefault(require("morgan"));
const headerManager_1 = require("./middlewares/headerManager");
const messageController_1 = require("./controllers/messageController");
const messageService_1 = require("./services/messageService");
const userController_1 = require("./controllers/userController");
const userService_1 = require("./services/userService");
const app = express_1.default();
exports.app = app;
setMiddlewares(app);
setRoutes(app);
setErrorHandling(app);
initMongoDatabaseConnection(process.env.MONGO_ATLAS_CONNECTION_LINK || "");
function setMiddlewares(app) {
    app.use(headerManager_1.headerManager);
    app.use(body_parser_1.default.urlencoded({ extended: false }));
    app.use(body_parser_1.default.json());
    app.use(morgan_1.default('dev'));
}
function setRoutes(app) {
    app.get("/", (req, res) => { res.send("StreetWord API server listening for requests"); });
    app.use('/messages', messageController_1.MessageRouter(new messageService_1.MessageService()));
    app.use('/users', userController_1.UserRouter(new userService_1.UserService()));
}
function setErrorHandling(app) {
    app.use((req, res, next) => {
        const err = new Error('Requested location not found');
        err.name = '404';
        next(err);
    });
    app.use((err, req, res, next) => {
        const responseData = {
            error: {
                message: err.message,
                statusCode: (err.name === '404') ? err.name : '500'
            }
        };
        res.status(parseInt(responseData.error.statusCode))
            .send(responseData);
    });
}
function initMongoDatabaseConnection(mongoConnectionLink) {
    mongoose_1.default.connect(mongoConnectionLink, { useNewUrlParser: true, config: { autoIndex: false } })
        .then(() => console.log('Succesfully connected to database'))
        .catch((err) => console.log(err));
}
