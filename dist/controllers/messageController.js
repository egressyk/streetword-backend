"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const checkAuth_1 = require("../middlewares/checkAuth");
class MessageController {
    constructor(messageService) {
        this.router = express_1.Router();
        this.messageService = messageService;
        this.setupControllers();
    }
    setupControllers() {
        this.router.route("/")
            .all(checkAuth_1.checkAuthentication)
            .get((req, res, next) => {
            this.messageService.getAllMessages()
                .then((result) => {
                res.status(200).json(this.convertToRestfulResponse(req, result));
            })
                .catch((error) => {
                next(error);
            });
        })
            .post((req, res, next) => {
            const newMessage = {
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                content: req.body.content,
                author: req.body.author,
            };
            this.messageService.creatMessage(newMessage)
                .then((result) => {
                res.status(201)
                    .json(this.convertToRestfulResponse(req, result));
            })
                .catch((error) => {
                next(error);
            });
        });
        this.router.route("/:messageId")
            .all(checkAuth_1.checkAuthentication)
            .get((req, res, next) => {
            this.messageService.getMessageByID(req.params.messageId)
                .then((result) => {
                if (result !== null) {
                    res.status(200).json(this.convertToRestfulResponse(req, result));
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No message with id: ${req.params.messageId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        })
            .delete((req, res, next) => {
            this.messageService.deleteMessage(req.params.messageId)
                .then((result) => {
                if (result !== null) {
                    res.status(200).json(this.convertToRestfulResponse(req, result));
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No message with id: ${req.params.messageId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        });
        this.router.route("/:messageId/vote")
            .all(checkAuth_1.checkAuthentication)
            .post((req, res, next) => {
            this.messageService.voteForMessage(req.params.messageId, req.body.userId, req.body.vote)
                .then((result) => {
                if (result !== null) {
                    res.status(200).json(this.convertToRestfulResponse(req, result));
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No message with id: ${req.params.messageId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        });
        this.router.route("/user/:userId")
            .all(checkAuth_1.checkAuthentication)
            .get((req, res, next) => {
            this.messageService.getMessagesByUserID(req.params.userId)
                .then((result) => {
                if (result !== null) {
                    let messages = (result.messages) ? result.messages : [];
                    res.status(200).json(this.convertToRestfulResponse(req, messages));
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No user with id: ${req.params.userId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        })
            .delete((req, res, next) => {
            this.messageService.deleteMessagesByUserID(req.params.userId)
                .then((result) => {
                console.log(result);
                if (result !== null) {
                    res.status(200).json({
                        message: `Deleted all messages of user: ${req.params.userId}`
                    });
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No user with id: ${req.params.userId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        });
    }
    convertToRestfulResponse(req, queryData) {
        if (queryData instanceof Array) {
            const self = this;
            return queryData.map((element) => {
                return self.convertToRestfulResponse.call(self, req, element);
            });
        }
        else {
            return {
                id: queryData._id,
                link: req.protocol + '://' + req.get('host') + req.baseUrl + '/' + queryData._id,
                latitude: queryData.latitude,
                longitude: queryData.longitude,
                content: queryData.content,
                author: {
                    id: queryData.author._id,
                    username: queryData.author.username,
                    link: req.protocol + '://' + req.get('host') + '/users' + '/' + queryData.author._id
                },
                creationDate: queryData.creationDate,
                votes: queryData.votes
            };
        }
    }
}
exports.MessageController = MessageController;
exports.MessageRouter = (messageService) => new MessageController(messageService).router;
