"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const jwt = __importStar(require("jsonwebtoken"));
const checkAuth_1 = require("../middlewares/checkAuth");
class UserController {
    constructor(messageService) {
        this.router = express_1.Router();
        this.userService = messageService;
        this.setupControllers();
    }
    setupControllers() {
        this.router.route("/")
            .get(checkAuth_1.checkAuthentication)
            .get((req, res, next) => {
            this.userService.getAllUsers()
                .then((result) => {
                res.status(200).json(this.convertToRestfulResponse(req, result));
            })
                .catch((error) => {
                next(error);
            });
        })
            .post((req, res, next) => {
            const userData = {
                username: req.body.username,
                token: req.body.token
            };
            this.userService.creatUserWithFacebookToken(userData)
                .then((result) => {
                res.status(201).json({
                    token: this.createUserToken(result)
                });
            })
                .catch((error) => {
                next(error);
            });
        });
        this.router.route("/login/facebook")
            .post((req, res, next) => {
            this.userService.loginUserWithFacebookToken(req.body.token)
                .then((result) => {
                if (result.length > 0) {
                    res.status(200).json({
                        token: this.createUserToken(result[0])
                    });
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No user for token: ${req.body.token}`
                        }
                    });
                    console.log("No user");
                }
            })
                .catch((error) => {
                next(error);
            });
        });
        this.router.route("/:userId")
            .all(checkAuth_1.checkAuthentication)
            .get((req, res, next) => {
            this.userService.getUserByID(req.params.userId)
                .then((result) => {
                if (result !== null) {
                    res.status(200).json(this.convertToRestfulResponse(req, result));
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No user with id: ${req.params.userId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        })
            .delete((req, res, next) => {
            this.userService.deleteUser(req.params.userId)
                .then((result) => {
                if (result !== null) {
                    res.status(200).json(this.convertToRestfulResponse(req, result));
                }
                else {
                    res.status(404).json({
                        error: {
                            message: `No user with id: ${req.params.userId}`
                        }
                    });
                }
            })
                .catch((error) => {
                next(error);
            });
        });
    }
    convertToRestfulResponse(req, queryData) {
        console.log(queryData);
        if (queryData instanceof Array) {
            const self = this;
            return queryData.map((element) => {
                return self.convertToRestfulResponse.call(self, req, element);
            });
        }
        else {
            return {
                id: queryData._id,
                username: queryData.username,
                email: queryData.email,
                creationDate: queryData.creationDate,
                link: req.protocol + '://' + req.get('host') + req.baseUrl + '/' + queryData._id,
                messages: queryData.messages
            };
        }
    }
    createUserToken(userData) {
        return jwt.sign({
            id: userData._id,
            email: userData.email
        }, process.env.JWT_KEY, {
            expiresIn: '1h'
        });
    }
}
exports.UserRouter = (userService) => new UserController(userService).router;
