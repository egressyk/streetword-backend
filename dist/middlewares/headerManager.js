"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function headerManager(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-type, Authorization');
    if (req.method == "OPTIONS") {
        res.sendStatus(200);
    }
    else {
        next();
    }
}
exports.headerManager = headerManager;
