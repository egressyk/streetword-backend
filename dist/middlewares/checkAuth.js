"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = __importStar(require("jsonwebtoken"));
function checkAuthentication(req, res, next) {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decodedTokenData = jwt.verify(token, process.env.JWT_KEY);
        req.body.userId = decodedTokenData.id;
        next();
    }
    catch (error) {
        return res.status(401).json({
            error: {
                message: "Authentication failed"
            }
        });
    }
}
exports.checkAuthentication = checkAuthentication;
